package com.example.handcontrol;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.ArrayList;
import java.util.UUID;

import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.ListView;
import android.content.Intent;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.ParcelUuid;
import android.widget.Button;

import ru.yandex.speechkit.Error;
import ru.yandex.speechkit.Language;
import ru.yandex.speechkit.OnlineModel;
import ru.yandex.speechkit.OnlineRecognizer;
import ru.yandex.speechkit.Recognition;
import ru.yandex.speechkit.Recognizer;
import ru.yandex.speechkit.RecognizerListener;
import ru.yandex.speechkit.SpeechKit;
import ru.yandex.speechkit.Track;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity implements RecognizerListener {

    private BluetoothAdapter myBluetooth = null;
    private CommunicationManager communicationManager = new CommunicationManager();
    private final String nameDeviceHand = "ArmProsthesis";
    private boolean stateConnectDevice = false;

    private OutputStream outputStream;
    private InputStream inStream;

    Thread workerThread;

    final int BUFFER_SIZE = 1024;
    byte[] buffer = new byte[BUFFER_SIZE];
    int bytes = 0;

    private static final String API_KEY_FOR_TESTS_ONLY = "069b6659-984b-4c5f-880e-aaedcfd84102";
    //private static final String API_KEY_FOR_TESTS_ONLY = "be227abb-a848-41a0-bf7d-a547e9d62068";
    private static final int REQUEST_PERMISSION_CODE = 31;

    private Recognizer recognizer;

    private ProgressBar progressBar;
    private TextView currentStatus;
    private TextView recognitionResult;
    ArrayList<String> ListGestures = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitRec();


        //TestComm();
        BluetoothConnect();

        if(stateConnectDevice)
        {
            CreateReadBluetooth();
            CommandsInit();
        }

        // По быдлански, ждем пока пакет не пришел и пока он не обработался. Надо как то исправить,
        // но хз как. Нужна многопоточность
        while (communicationManager.DataPackages.size() == 0);

        ArrayList<Integer> currentPackage = communicationManager.DataPackages.get(0);
        HandlePackage(currentPackage);

//        String[] commands = {"Сжать", "Разжать", "Поворот", "Клавиатура", "Класс", "Рок-н-ролл", "Окей"};
//        ListCommandInit(commands);
    }

    private void InitRec()
    {
        try {
            SpeechKit.getInstance().init(this, API_KEY_FOR_TESTS_ONLY);
            SpeechKit.getInstance().setUuid(UUID.randomUUID().toString());
        } catch (SpeechKit.LibraryInitializationException ignored) {
            //do not ignore in a prod version!
        }

        recognizer = new OnlineRecognizer.Builder(Language.RUSSIAN, OnlineModel.QUERIES,
                MainActivity.this).build();


        findViewById(R.id.start_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecognizer();
            }
        });

        findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recognizer.cancel();
            }
        });

        progressBar = findViewById(R.id.voice_power_bar);
        currentStatus = findViewById(R.id.current_state);
        recognitionResult = findViewById(R.id.result);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != REQUEST_PERMISSION_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length == 1 && grantResults[0] == PERMISSION_GRANTED) {
            startRecognizer();
        } else {
            updateStatus("Record audio permission was not granted");
        }
    }

    @Override
    public void onRecordingBegin(@NonNull Recognizer recognizer) {
        updateStatus("Recording begin");
        updateProgress(0);
        updateResult("");
    }

    @Override
    public void onSpeechDetected(@NonNull Recognizer recognizer) {
        updateStatus("Speech detected");
    }

    @Override
    public void onSpeechEnds(@NonNull Recognizer recognizer) {
        updateStatus("Speech ends");
    }

    @Override
    public void onRecordingDone(@NonNull Recognizer recognizer) {
        updateStatus("Recording done");
    }

    @Override
    public void onPowerUpdated(@NonNull Recognizer recognizer, float power) {
        updateProgress((int) (power * progressBar.getMax()));
    }

    @Override
    public void onPartialResults(@NonNull Recognizer recognizer, @NonNull Recognition recognition, boolean eOfU) {
        updateStatus("Partial results " + recognition.getBestResultText() + " endOfUtterrance = " + eOfU);

        if (eOfU) {
            updateResult(recognition.getBestResultText());


            try {
                SendGesture(GetGesture(removeLastChar(recognition.getBestResultText())));
            } catch (NoSuchElementException e) {
                Toast.makeText(getApplicationContext(), "Не найден жест " + recognition.getBestResultText(), Toast.LENGTH_LONG).show();
            }


        }
    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    private String GetGesture(String text) throws NoSuchElementException {
        for (String guestur: ListGestures
             ) {
            if(guestur.toLowerCase().equals(text.toLowerCase()))
            {
                return guestur;
            }
        }

        throw new NoSuchElementException();
    }

    @Override
    public void onRecognitionDone(@NonNull Recognizer recognizer) {
        updateStatus("Recognition done");
        updateProgress(0);
    }

    @Override
    public void onRecognizerError(@NonNull Recognizer recognizer, @NonNull Error error) {
        updateStatus("Error occurred " + error);
        updateProgress(0);
        updateResult("");
    }

    @Override
    public void onMusicResults(@NonNull Recognizer recognizer, @NonNull Track track) {
    }


    private void startRecognizer() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, RECORD_AUDIO) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO}, REQUEST_PERMISSION_CODE);
        } else {
            recognizer.startRecording();
        }
    }

    private void updateResult(String text) {
        recognitionResult.setText(text);
    }

    private void updateStatus(final String text) {
        currentStatus.setText(text);
    }

    private void updateProgress(int progress) {
        progressBar.setProgress(progress);
    }

    private void ListCommandInit(String[] commands)
    {
        // находим список
        ListView ListCommand = (ListView) findViewById(R.id.ListCommand);

        ListCommand.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ListView ListCommand = findViewById(R.id.ListCommand);
                SendGesture((String) ListCommand.getItemAtPosition(position));
            }
        });

        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, commands);

        // присваиваем адаптер списку
        ListCommand.setAdapter(adapter);

        ListGestures.clear();
        ListGestures.addAll(Arrays.asList(commands));
    }

    private  void SendGesture(String gesture)
    {
        try {
            byte[] dataTx = CommunicationManager.ExecuteTheCommand(gesture);
            WriteBluetooth(dataTx);
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "Невозможно отправить комманду", Toast.LENGTH_LONG).show();
        }
        catch (NullPointerException e)
        {
            Toast.makeText(getApplicationContext(), "Протез не подключен", Toast.LENGTH_LONG).show();
        }
    }

    private void CreateReadBluetooth()
    {
        workerThread = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted())
                {
                    try {
                        bytes = inStream.read(buffer, bytes, BUFFER_SIZE - bytes);
                        for (Byte data:buffer)
                        {
                            communicationManager.ReceivePackageAdd(data);
                        }
                        // Toast.makeText(getApplicationContext(), "" + buffer[0], Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        workerThread.start();

//        workerThread = new Thread(new Runnable() {
//            public void run() {
//                while(!Thread.currentThread().isInterrupted())
//                {
//                    try {
//                        if(communicationManager.DataPackages.size() > 0)
//                        {
//                            ArrayList<Integer> currentPackage = communicationManager.DataPackages.get(0);
//                            HandlePackage(currentPackage);
//                        }
//                    } catch (NullPointerException  e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//        workerThread.start();
    }


    private void HandlePackage(ArrayList<Integer> currentPackage)
    {
        int headPackage = currentPackage.get(0);
        currentPackage.remove(0);

        switch (headPackage)
        {
            case 0x21: // CommandActionListAnswer
                WordConvertWord(currentPackage);
                break;
        }

    }

    private void WordConvertWord(ArrayList<Integer> currentPackage)
    {

        int countWords = (currentPackage.size())/20;

        String[] commands = new  String[countWords];

        for(int i = 0; i < countWords; i++)
        {

            StringBuffer sb = new StringBuffer();

            ArrayList<Byte> codeChBufferTmp = new ArrayList<>();
            for (int j = 0; j < 20; j++)
            {
                int codeCh = (int)currentPackage.get(i*20 + j);

                if(codeCh > 127)
                    codeCh = codeCh - 256;

                if(codeCh == 0)
                    break;

                codeChBufferTmp.add((byte) codeCh);
            }


            byte[] codeChBuffer = new byte[codeChBufferTmp.size()];
            for(int k = 0; k < codeChBufferTmp.size(); k++)
            {
                codeChBuffer[k] = codeChBufferTmp.get(k);
            }


            String word;
            try
            {
                word = new String(codeChBuffer, "windows-1251");
                commands[i] =  word;
            }
            catch (java.io.UnsupportedEncodingException exception)
            {
                System.out.println(exception.getMessage());
            }

//            try
//            {
//                String utf8String = new String("абвгд".getBytes(), "UTF-8");
//                String ansiString = new String(utf8String.getBytes("UTF-8"), "windows-1251");
//            }
//            catch (java.io.UnsupportedEncodingException exception)
//            {
//                System.out.println(exception.getMessage());
//            }

        }
        ListCommandInit(commands);
    }

    private void CommandsInit()
    {
        try {
            byte[] array_send = CommunicationManager.ActionListRequestCommand();
            WriteBluetooth(array_send);
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "Невозможно отправить строку", Toast.LENGTH_LONG).show();
        }
    }

    private void  BluetoothConnect()
    {
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null)
        {
            //Show a mensag. that thedevice has no bluetooth adapter
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            //finish apk
            finish();
        }
        else
        {
            if (myBluetooth.isEnabled())
            {
                ConnectToDeviceHand();
            }
            else
            {
                //Ask to the user turn the bluetooth on
                Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnBTon,1);
                ConnectToDeviceHand();
            }
        }
    }

    private void ConnectToDeviceHand()
    {
        Set<BluetoothDevice> pairedDevices;
        pairedDevices = myBluetooth.getBondedDevices();

        if (pairedDevices.size()>0)
        {
            for(BluetoothDevice bt : pairedDevices)
            {
                if(bt.getName().equals(nameDeviceHand))
                {
                    ParcelUuid[] uuids = bt.getUuids();
                    //while (true)
                    //{
                        Toast.makeText(getApplicationContext(), uuids[0].toString(), Toast.LENGTH_LONG).show();
                    //}
                    try {
                        BluetoothSocket socket = bt.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                        socket.connect();
                        outputStream = socket.getOutputStream();
                        inStream = socket.getInputStream();
                        stateConnectDevice = true;
                    }
                    catch (java.io.IOException e)
                    {
                        Toast.makeText(getApplicationContext(), "Невозможно подключиться к устройству", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    public void WriteBluetooth(String s) throws IOException {
        outputStream.write(s.getBytes());
    }

    public void WriteBluetooth(byte[] data) throws IOException {
        outputStream.write(data);
    }

}
