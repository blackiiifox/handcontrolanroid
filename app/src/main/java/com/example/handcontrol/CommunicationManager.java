package com.example.handcontrol;

import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Object;


public class CommunicationManager {
    // Адреса устройств протокольного уровня
    public static byte addressPC = 0x00;
    public static byte addressHand = 0x01;
    public static byte addressVoice = 0x02;

    private static byte CommandSave = 0x15;        // Сохранить комманды
    private static byte CommandExecByName = 0x16;   // Выполнить записанную комануду
    private static byte CommandExecByMotion = 0x17;        // Выполнить комманду по полученным данным
    private static byte CommandExexRaw = 0x18;     // Установить на все двигатели данное значение
    private static byte CommandSaveToVoice = 0x19; // Сохранить комманду в устройство распознования речи
    private static byte CommandActionListRequest = 0x20; // Запросить список команд устройства(только имя)
    private static byte CommandActionListAnswer = 0x21; // Ответ на зпрос команд с руки

    private static Byte[] startFiled = new Byte[] { '1', 'N', '7', 'R', 'O', 'I', 'N', 'm' };
    private static Byte[] endFiled = new Byte[] { 'R', '{', 'D', '9', '8', 'V', '8', '9' };
    private static Byte[] versionProtocol = new Byte[] { '0', '1'};

    public ArrayList<ArrayList<Integer>> DataPackages = new ArrayList<>();
    private ArrayList<Byte> receivePacket = new ArrayList<>();
    private boolean stateStartReceive = false;
    private long LastReadTime = 0;
    private int countMainField = 0;
    private int FieldCRC8 = 0;

    public static byte[] ActionListRequestCommand()
    {
        ArrayList<Byte> data = new ArrayList<>();
        data.add(CommandActionListRequest);
        Byte[] packageTransmit_tmp = CreatePackage(addressHand, data);

        byte[] packageTransmit = new byte[packageTransmit_tmp.length];
        for (int i = 0; i < packageTransmit_tmp.length; i++)
        {
            packageTransmit[i] = packageTransmit_tmp[i];
        }

        return packageTransmit;
    }

    public static byte[] ExecuteTheCommand(String nameCommand)
    {
        ArrayList<Byte> data = new ArrayList<>();
        data.add(CommandExecByName);

        byte[] byteName = new byte[0];
        try {
            byteName = nameCommand.getBytes("cp1251");
        }
        catch (java.io.UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        if (byteName.length == 20)
        {
            byteName[18] = '\0';
            byteName[19] = '\0';
        }

        for(int i = 0; i < 20; i++)
        {
            if(i > byteName.length - 1)
                data.add((byte)'\0');
            else
                data.add(byteName[i]);
        }

        Byte[] packageTransmit_tmp = CreatePackage(addressHand, data);

        byte[] packageTransmit = new byte[packageTransmit_tmp.length];
        for (int i = 0; i < packageTransmit_tmp.length; i++)
        {
            packageTransmit[i] = packageTransmit_tmp[i];
        }

        return packageTransmit;
    }

    private static Byte[] CreatePackage(byte distAddress, ArrayList<Byte> dataField)
    {
        // Стартовая константа
        ArrayList<Byte> new_package = new ArrayList<>();
        new_package.addAll(Arrays.asList(startFiled));

        // Заполнения поля информации пакета
        ArrayList<Byte> infoField = new ArrayList<>();
        infoField.addAll(Arrays.asList(versionProtocol));
        infoField.add(addressPC);
        infoField.add(distAddress);

        int countField = dataField.size();
        // Заполнение поля размера информационного пакета
        Byte[] countFieldByte = new Byte[4];
        countFieldByte[3] = (byte)(countField & 0x000000FF);
        countFieldByte[2] = (byte)((countField & 0x0000FF00) >> 8);
        countFieldByte[1] = (byte)((countField & 0x00FF0000) >> 16);
        countFieldByte[0] = (byte)((countField & 0xFF000000) >> 24);
        infoField.addAll(Arrays.asList(countFieldByte));

        // Создание основного контейнера данных(без CRC,стартовых и стоповых констант)
        ArrayList<Byte> mainField = infoField; // info + data field

        // Integer[] bytes = dataField.toArray(new Integer[dataField.size()]);
        Integer[] bytes = new Integer[dataField.size()];
        for(int i = 0; i < dataField.size(); i++)
        {
            int data = (int)dataField.get(i);
            if(data < 0)
                data += 256;
            bytes[i] = data;
        }

        Byte crc8 = (byte)(CRC8.Calculate(bytes, (byte) 0)&0xFF);

        mainField.addAll(dataField);
        mainField.add(crc8);

        // Добавление в конечный пакет основного контейнера, crc кода и стоповой последовательности
        new_package.addAll(mainField);
        new_package.addAll(Arrays.asList(endFiled));

        Byte[] bytes_package = new_package.toArray(new Byte[new_package.size()]);

        return bytes_package;
    }

    public void ReceivePackageAdd(Byte data)
    {
//        if(System.currentTimeMillis() - LastReadTime > 3000)
//        {
//            ClearDataReceive();
//        }
        receivePacket.add(data);
        HandleReceiveData();
        LastReadTime = System.currentTimeMillis();
    }

    private void HandleReceiveData()
    {
        if(!stateStartReceive)
        {
            if (receivePacket.size() == 8)
            {
                if(CheckStartField())
                    stateStartReceive = true;
                else
                    receivePacket.remove(0);
            }
        }
        else {

            // Проверка версии протокола
            if (receivePacket.size() == 10)
            {
                if(!CheckVersionField())
                {
                    ClearDataReceive();
                }
            }

            // Проверка адреса
            if (receivePacket.size() == 12)
            {
                if(!CheckAddress())
                {
                    ClearDataReceive();
                }
            }

            // Считывание размера поля данных
            if (receivePacket.size() == 16)
            {
                 countMainField = ReadCountMainField();
            }

            // Принимаем CRC8
            if(receivePacket.size() == countMainField + 17)
            {
                FieldCRC8 = receivePacket.get(receivePacket.size() - 1);
                if (FieldCRC8 < 0) {
                    FieldCRC8 = FieldCRC8 + 256;
                }
            }

            // Принимакем стоповое поле
            if(receivePacket.size() == countMainField + 17 + 8)
            {
                if(!CheckStopField())
                {
                    ClearDataReceive();
                }
                else
                {
                    ArrayList<Integer> newPackage = new ArrayList<>();
                    for(int i = 16; i < countMainField + 16; i++)
                    {
                        int data = (int)receivePacket.get(i);
                        if(data<0)
                            data=256+data;
                        newPackage.add(data);
                    }

                    Integer[] bytes = newPackage.toArray(new Integer[newPackage.size()]);
                    int crc8 = CRC8.Calculate(bytes, (byte) 0);

                    if(crc8 == FieldCRC8) {
                        DataPackages.add(newPackage);
                    }
                    ClearDataReceive();
                }
            }

        }
    }

    private boolean CheckStartField()
    {
        for (int i = 0; i < 8; i++)
        {
            if(!receivePacket.get(i).equals(startFiled[i]))
                return false;
        }
        return true;
    }

    private boolean CheckVersionField()
    {
        return receivePacket.get(8).equals(versionProtocol[0])&&receivePacket.get(9).equals(versionProtocol[1]);
    }

    private boolean CheckAddress()
    {
        return receivePacket.get(11).equals(addressVoice);
    }

    private int ReadCountMainField()
    {
        int countDataField;

        countDataField = receivePacket.get(receivePacket.size()-1);
        countDataField |= (receivePacket.get(receivePacket.size()-2))<<8;
        countDataField |= (receivePacket.get(receivePacket.size()-3))<<16;
        countDataField |= ((receivePacket.get(receivePacket.size()-4)))<<24;

        if(countDataField < 0) {
            countDataField += 256;
        }

        return countDataField;
    }

    private boolean CheckStopField()
    {
        for (int i = 0; i < 8; i++)
        {
            if(!receivePacket.get(receivePacket.size() - 8 + i).equals(endFiled[i]))
                return false;
        }
        return true;
    }

    private void ClearDataReceive()
    {
        countMainField = 0;
        stateStartReceive = false;
        receivePacket.clear();
    }
}


